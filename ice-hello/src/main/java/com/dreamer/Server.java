package com.dreamer;

import com.dreamer.service.HelloWorldI;

/**
 * Hello world!
 *
 */
public class Server 
{
    public static void main( String[] args )
    {
    	int status = 0;
        Ice.Communicator communicator = null;
        try {
			communicator = Ice.Util.initialize(args);
			Ice.ObjectAdapter adapter = communicator.createObjectAdapterWithEndpoints("SimplePrinterAdapter", "default -p 10000");
			Ice.Object object = new HelloWorldI();
			adapter.add(object, communicator.stringToIdentity("SimplePrinter"));
			adapter.activate();
			communicator.waitForShutdown();
		} catch (Exception e) {
			e.printStackTrace();
			status = 1;
		}finally {
			if (communicator != null) {
				communicator.destroy();
			}
		}
        
        System.exit(status);
    }
}