package com.dreamer.service;

import Ice.Current;
import firstHelloWorld._HelloWorldDisp;

public class HelloWorldI extends _HelloWorldDisp {

	@Override
	public void greeting(String value, Current __current) {
		System.out.println("Greeting: "+value);
	}

	@Override
	public void testAnotherOperation(String value, Current __current) {
		System.out.println("Value was passed: "+value);
	}

}
