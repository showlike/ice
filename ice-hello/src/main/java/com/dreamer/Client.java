package com.dreamer;

import firstHelloWorld.HelloWorldPrx;
import firstHelloWorld.HelloWorldPrxHelper;

public class Client {

	public static void main(String[] args) {
		int status = 0;
		Ice.Communicator ic = null;
		try {
			ic = Ice.Util.initialize(args);
			Ice.ObjectPrx base = ic.stringToProxy("SimplePrinter:default -p 10000");
			HelloWorldPrx printer = HelloWorldPrxHelper.checkedCast(base);
			if (printer == null) {
				throw new Error("Invalid proxy");
			}
			
			printer.greeting("Hello World!");
		} catch (Exception e) {
			e.printStackTrace();
			status = 1;
		}finally {
			if (ic != null) {
				ic.destroy();
			}
		}
		
		System.exit(status);
	}
}