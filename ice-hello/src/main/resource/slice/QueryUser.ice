[["java:package:com.dreamer"]]
module fileice{
	class User{
		string name;
		int age;
	};
	
	interface UserService{
		User execute(User user);
	};
}